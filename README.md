# COMP4418 Assignment 1

Determining a value of `C` for which satisfiability becomes hard to determine.

# Dependencies

-   python3
-   pipenv
-   minisat: https://github.com/niklasso/minisat

# Executing program
 
run `pipenv isntall` at least once.
`pipenv run q3` to create the graph.

#!/bin/sh

if test $# -ne 1
then
    echo "Usage: ./is_satisfiable {FILE}"
    exit 0
fi

file=$1
time=`minisat "$file" | grep "CPU time" | cut -d ':' -f2 | cut -d ' ' -f2`

echo $time

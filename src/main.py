import subprocess
import os
import shutil
import random
import matplotlib.pyplot as plt

FOLDER = "./cnf_files"

# generates CNF files in 3-stat format
# creates them in /cnf_files directory
def make_files(start, end):
    os.mkdir(FOLDER)

    for literal_count in range(start, end):
        for clause_count in range (start, end):

            # skip the file if it's impossible to include
            # all the literals in the number of clauses
            if (literal_count > clause_count * 3):
                continue
            
            make_file(literal_count, clause_count)
        
# will generate a CNF file with the given number of literals and clauses
def make_file(literal_count, clause_count):
    f = open(f"cnf_files/cnf_{literal_count}_{clause_count}.cnf", "a")
    f.write(make_file_contents(literal_count, clause_count))
    f.close()

def make_file_contents(literal_count, clause_count):
    header = f"c CNF file with {literal_count} propositional variables and {clause_count} clauses\n"
    beginning = f"p cnf {literal_count} {clause_count}\n"
    clauses = make_clauses(literal_count, clause_count)    

    return header + beginning + "".join(clauses)


# we want to ensure that every literal is included
# at least once in all of the clauses. This means
# clause_count * 3 >= literal_count
def make_clauses(literal_count, clause_count):
    clauses = []

    # keeps track of literals we haven't used
    literals_left = list(range(0, literal_count))

    for x in range(0, clause_count):    
        clauses.append(make_clause(literal_count, literals_left))

    return clauses


def make_clause(literal_count, literals_left):
    # bit messy, just hard-coding some edge cases
    if (len(literals_left) == 1):
        first = make_negative(literals_left[0])
        second = make_negative(random.randint(0, literal_count-1))
        third = make_negative(random.randint(0, literal_count-1))
        literals_left = []

    elif (len(literals_left) == 2):
        first = make_negative(literals_left[0])
        second = make_negative(literals_left[1])
        third = make_negative(random.randint(0, literal_count-1))
        literals_left = []

    elif (len(literals_left) > 2):        
        first = random.choice(literals_left)
        literals_left.remove(first)

        second = random.choice(literals_left)
        literals_left.remove(second)

        third = random.choice(literals_left)
        literals_left.remove(third)

        first = make_negative(first)
        second = make_negative(second)
        third = make_negative(third)

    # literals_left.length == 0
    else:
        first = make_negative(random.randint(0, literal_count-1))
        second = make_negative(random.randint(0, literal_count-1))
        third = make_negative(random.randint(0, literal_count-1))

    return f" {first} {second} {third} 0\n"

# will randomly determine whether to int make negative
def make_negative(num):
    if random.randint(0,100) >= 50:
        return -1 * num
    else:
        return num


# from: https://stackoverflow.com/questions/185936/how-to-delete-the-contents-of-a-folder
def clean_files():    
    for filename in os.listdir(FOLDER):
        file_path = os.path.join(FOLDER, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def get_data():
    # list of tuples where the first element is the c-value
    # and the second element is the time to determine satisfiability
    data = []

    for filename in os.listdir(FOLDER):
        output = subprocess.run(
            [
                "./src/is_satisfiable.sh",
                FOLDER + "/" + filename    
            ],
            capture_output=True,
        )
        time = float(output.stdout.decode("utf-8"))
        c_value = calculate_c_value_from_filename(filename)
        
        data.append((c_value, time))

    return data

def calculate_c_value_from_filename(filename):
    filename = filename.replace(".cnf", "")
    arr = filename.split("_")
    literal_count = float(arr[1])
    clause_count = float(arr[2])
    return clause_count / literal_count # the c-value

def plot_data(data):
    c_values = []
    times = []
    for c_value, time in data:
        c_values.append(c_value)
        times.append(time)

    plt.scatter(c_values, times)
    plt.xlabel("C-Value")
    plt.ylabel("Time")
    plt.show()

    

def main():
    make_files(2, 20)
    data = get_data()
    plot_data(data)
    clean_files()


if __name__ == "__main__":
    main()
